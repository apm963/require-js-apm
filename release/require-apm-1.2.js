(function(scope) {
	// IIFE for scope
	
	const cache = {};
	const pending = {}; // TODO: Implement a way to know that a required script has already been requested but is not yet satisfied
	const targets = ['ESNext', 'ES7', 'ES6', 'ES5', 'ES3'];
	const default_options = {
		overrideBrowserCache: true,
		useInternalCache: true,
		target: 'ES6', // ES6 | ES7
		mapRoot: false, // false || path
	};
	
	function read_file(url, options = {}) {
		return new Promise(function(resolve, reject) {
			
			options.cache = (typeof(options.cache) !== 'undefined' ? options.cache : true);
			
			let req = new XMLHttpRequest();
			
			url += (!(/\.\w+(\?|$)/).test(url) ? '.js' : '');
			
			if (!options.cache) {
				url += ((/\?/).test(url) ? "&" : "?") + '_=' + (new Date()).getTime();
			}
			
			req.open('GET', url);
			
			if (typeof(options.dataType) !== 'undefined') {
				req.responseType = options.dataType;
			}
			
			req.onload = function() {
				// This is called even on 404 etc
				// so check the status
				if (req.status == 200) {
					// Resolve the promise with the response text
					resolve(req.response);
				}
				else {
					// Otherwise reject with the status text
					// which will hopefully be a meaningful error
					reject(Error(req.statusText));
				}
			};
			
			// Handle network errors
			req.onerror = function() {
				reject(Error('Network Error'));
			};
			
			// Make the request
			req.send();
			
			return;
		});
	}
	
	async function require_single(name, options = default_options) {
		
		if (options.mapRoot !== false && name.substr(0, 2) === './') {
			name = options.mapRoot+name.substr(2);
		}
		
		if (options.useInternalCache && typeof cache[name] !== 'undefined') {
			//console.log(`Using ${name} from cache`);
			return cache[name];
		}
		
		let code;
		
		try {
			code = await read_file(name, {cache: !options.overrideBrowserCache})
		}
		catch (err) {
			throw err;
		}
		
		// Switch on target
		switch (options.target.toString().toLowerCase()) {
			case 'esnext': // The newest
			case 'es7':
				code = `return (async function() { \n ${code} \n });`;
				break;
			case 'es6': // Fallthrough
			default:
				// Grabbed this from Typescript's output with target set to ES6
				code = `
				var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
					return new (P || (P = Promise))(function (resolve, reject) {
						function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
						function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
						function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
						step((generator = generator.apply(thisArg, _arguments || [])).next());
					});
				};
				return (function() {
					return __awaiter(this, void 0, void 0, function* () { \n ${code} \n });
				});`;
				break;
		}
		
		let funcBuilder = new Function('exports, module, scope', code);
		let exports = {}, module = {exports: exports};
		
		let func = funcBuilder(exports, module, scope);
		await func();
		
		cache[name] = module.exports;
		
		return module.exports;
	}
	
	scope.require = (async function(name_mixed, options = default_options) {
		
		let return_array = (typeof name_mixed === 'object' && typeof name_mixed.length !== 'undefined');
		name_mixed = (return_array ? name_mixed : [name_mixed]);
		
		for (let key in default_options) {
			options[key] = (typeof options[key] !== 'undefined' ? options[key] : default_options[key]);
		}
		
		let promises = [];
		
		for (let name of name_mixed) {
			promises.push( require_single(name, options) );
		}
		
		const exports = [];
		
		try {
			exports = await Promise.all(promises);
		}
		catch (err) {
			//console.log(err);
		}
		
		/*
		let exports_assoc = {};
		
		let i = 0;
		
		for (let name of name_mixed) {
			exports_assoc[name] = exports[ i++ ];
		}
		*/
		
		return (return_array ? exports : exports.pop());
	});
	
	// Expose additional methods
	scope.require.setRelativeRoot = (function(mapRoot) {
		
		if (mapRoot.substr(-1) !== '/') {
			mapRoot += '/';
		}
		
		default_options.mapRoot = mapRoot;
		
		return this;
	});
	
	// Expose properties
	scope.require.defaults = default_options;
	scope.require.targets = targets;
	
})(this);