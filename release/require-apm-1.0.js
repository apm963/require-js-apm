(function(scope) {
	// IIFE for scope
	
	function read_file(url, options = {}) {
		return new Promise(function(resolve, reject) {
			
			options.cache = (typeof(options.cache) !== 'undefined' ? options.cache : true);
			
			let req = new XMLHttpRequest();
			
			url += (!(/\.\w+(\?|$)/).test(url) ? '.js' : '');
			
			if (!options.cache) {
				url += ((/\?/).test(url) ? "&" : "?") + '_=' + (new Date()).getTime();
			}
			
			req.open('GET', url);
			
			if (typeof(options.dataType) !== 'undefined') {
				req.responseType = options.dataType;
			}
			
			req.onload = function() {
				// This is called even on 404 etc
				// so check the status
				if (req.status == 200) {
					// Resolve the promise with the response text
					resolve(req.response);
				}
				else {
					// Otherwise reject with the status text
					// which will hopefully be a meaningful error
					reject(Error(req.statusText));
				}
			};
			
			// Handle network errors
			req.onerror = function() {
				reject(Error('Network Error'));
			};
			
			// Make the request
			req.send();
			
			return;
		});
	}
	
	const cache = {};
	const pending = {}; // TODO: Implement a way to know that a required script has already been requested but is not yet satisfied
	
	scope.require = (async function(name, options = {}) {
		
		options.overrideBrowserCache = (typeof overrideBrowserCache !== 'undefined' ? overrideBrowserCache : true);
		
		if (typeof cache[name] !== 'undefined') {
			//console.log(`Using ${name} from cache`);
			return cache[name];
		}
		
		let code = await read_file(name, {cache: !options.overrideBrowserCache});
		
		// Pad code with async function so it can take advantage of await functionality
		if (true) {
			code = `return (async function() { ${code} });`;
		}
		
		let funcBuilder = new Function('exports, module, scope', code);
		let exports = {}, module = {exports: exports};
		
		let func = funcBuilder(exports, module, scope);
		await func();
		
		cache[name] = module.exports;
		
		return module.exports;
	});
	
})(this);