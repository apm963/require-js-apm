(function(scope) {
	// IIFE for scope
	
	const cache = {};
	const pending = {};
	const targets = ['ESNext', 'ES7', 'ES6'];
	const default_options = {
		overrideBrowserCache: true,
		useInternalCache: true,
		target: 'ES6', // ES6 | ES7
		mapRoot: false, // false || path
		scriptDir: false, // false || path
		debug: false,
	};
	const exposed_lib_name = (scope.require_apm_lib_name || 'require');
	
	function read_file(url, options = {}) {
		return new Promise(function(resolve, reject) {
			
			options.cache = (typeof(options.cache) !== 'undefined' ? options.cache : true);
			
			let req = new XMLHttpRequest();
			
			url += (!(/\.\w+(\?|$)/).test(url) ? '.js' : '');
			
			if (!options.cache) {
				url += ((/\?/).test(url) ? "&" : "?") + '_=' + (new Date()).getTime();
			}
			
			req.open('GET', url);
			
			if (typeof(options.dataType) !== 'undefined') {
				req.responseType = options.dataType;
			}
			
			req.onload = function() {
				// This is called even on 404 etc
				// so check the status
				if (req.status == 200) {
					// Resolve the promise with the response text
					resolve(req.response);
				}
				else {
					// Otherwise reject with the status text
					// which will hopefully be a meaningful error
					reject(Error(req.statusText));
				}
			};
			
			// Handle network errors
			req.onerror = function() {
				reject(Error('Network Error'));
			};
			
			// Make the request
			req.send();
			
			return;
		});
	}
	
	async function executeCode(code, exports, module, scope, target) {
		// executeCode and executeCodeGenerator were experimental solutions to minify the existing code and solve the source map issue
		
		//eval(code);
		
		//for (let v of executeCodeGenerator(code, exports, module, scope, target)) { console.log(v); }
		
		//new Function('exports, module, scope', code);
		
		if (target != 'es7' && target != 'esnext') {
			
			let assembledCode = '(function* (exports, module, scope) {'+code+"\n"+'})(exports, module, scope);';
			
			let generator = eval(assembledCode);
			
			for (let x of generator) {
				if (x instanceof Promise) {
					console.log('start');
					await x;
					console.log('done');
				}
			}
			
		}
		
	}
	
	function* executeCodeGenerator(code, exports, module, scope, target) {
		eval(code);
	}
	
	async function requireSingle(name, options = default_options) {
		
		if (options.mapRoot !== false && name.substr(0, 2) === './') {
			//console.log([name, options.mapRoot, options.mapRoot+name.substr(2)]);
			name = options.mapRoot+name.substr(2);
		}
		
		if (options.useInternalCache && typeof cache[name] !== 'undefined') {
			if (options.debug) {
				console.log(`Using ${name} from cache`);
			}
			return cache[name];
		}
		
		if (typeof pending[name] !== 'undefined') {
			if (options.debug) {
				console.log(`${name} has already been requested but has not been retrieved yet`);
			}
			// Return a Promise that gets resolved immediately so the await in the calling context resolves
			return new Promise( resolve => resolve(pending[name]) );
		}
		
		let pending_resolve;
		pending[name] = new Promise( resolve => { pending_resolve = resolve; });
		
		let dir = name.substr(0, name.lastIndexOf('/'));
		let code, assembledCode = '';
		
		try {
			code = await read_file(name, {cache: !options.overrideBrowserCache})
		}
		catch (err) {
			throw err;
		}
		
		// Switch on target
		switch (options.target.toString().toLowerCase()) {
			case 'esnext': // The newest
			case 'es7':
				assembledCode = `return (async function() { \n ${code} \n });`;
				break;
			case 'es6': // Fallthrough
			default:
				// Grabbed this from Typescript's output with target set to ES6
				assembledCode = `
				require.setRelativeRoot('${dir}');
				var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
					return new (P || (P = Promise))(function (resolve, reject) {
						function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
						function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
						function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
						step((generator = generator.apply(thisArg, _arguments || [])).next());
					});
				};
				return (function() {
					return __awaiter(this, void 0, void 0, function* () { \n ${code} \n });
				});`;
				break;
		}
		
		let funcBuilder = new Function('exports, module', assembledCode);
		let exports = {}, module = {exports: exports};
		
		let func = funcBuilder.call(scope, exports, module);
		await func();
		
		if (true) { // TODO: Option to await exported Promises
			
			let pendingExports = [], exportsMap = [];
			
			for (let exportName of Object.keys(module.exports)) {
				if (module.exports[exportName] instanceof Promise) {
					pendingExports.push(module.exports[exportName]);
					exportsMap.push(exportName);
				}
			}
			
			let resolvedPromises = await Promise.all(pendingExports);
			
			for (let i = 0; i < resolvedPromises.length; i++) {
				module.exports[ exportsMap[i] ] = resolvedPromises[i];
			}
			
		}
		
		/* This was an experimental solution to the source map problem + code abstraction
		let exports = {}, module = {exports: exports};
		await executeCode(code, exports, module, scope, options.target.toString().toLowerCase());
		*/
		
		// Cache execution results
		cache[name] = module.exports;
		
		// Resolve Promise for other pending requires and unset
		pending_resolve(cache[name]);
		delete pending[name];
		
		// Return exports
		return cache[name];
	}
	
	async function require(name_mixed, options = default_options) {
		
		let return_array = (typeof name_mixed === 'object' && typeof name_mixed.length !== 'undefined');
		name_mixed = (return_array ? name_mixed : [name_mixed]);
		
		for (let key in default_options) {
			options[key] = (typeof options[key] !== 'undefined' ? options[key] : default_options[key]);
		}
		
		let promises = [];
		
		for (let name of name_mixed) {
			promises.push( requireSingle(name, options) );
		}
		
		let exports = [];
		
		try {
			// TODO: Return individual resource element as null instead of failing all
			exports = await Promise.all(promises);
		}
		catch (err) {
			//console.log(err);
			throw err;
		}
		
		return (return_array ? exports : exports.pop());
	}
	
	function requireWrapper(...args) {
		
		// This wrapper allows the use of Typescript's native import functionality
		// with the ability to await for a single or group of imports to resolve.
		
		let promise = require(args[0], args[1]);
		
		// For now, arrays do not support magic method bindings
		if (typeof args[0] === 'object') {
			return promise;
		}
		
		return new Proxy(
			promise,
			{
				get: function(target, name) {
					
					if (typeof name !== 'string' && typeof name !== 'number') {
						return undefined;
					}
					
					/*
					if (typeof promise[name] !== 'undefined') {
						return promise[name];
					}
					*/
					
					if (typeof target[name] !== 'undefined') {
						return target[name];
					}
					
					return new Promise(function(resolve) {
						
						promise.then(function(r) {
							
							for (let key in r) {
								
								if (typeof target[key] !== 'undefined') {
									continue;
								}
								
								target[key] = r[key];
								
							}
							
							return resolve( r[name] );
						});
						
					});
				}
			}
		);
	}
	
	// Expose main function
	scope[exposed_lib_name] = requireWrapper;
	
	function setRelativeRoot(mapRoot) {
		
		if (typeof mapRoot === 'string' && mapRoot.substr(-1) !== '/') {
			mapRoot += '/';
		}
		
		default_options.mapRoot = mapRoot;
		
		return scope[exposed_lib_name];
	}
	
	function setScriptDir(scriptDir) {
		
		if (typeof scriptDir === 'string' && scriptDir.substr(-1) !== '/') {
			scriptDir += '/';
		}
		
		default_options.scriptDir = scriptDir;
		
		return scope[exposed_lib_name];
	}
	
	// Expose additional methods
	scope[exposed_lib_name].setRelativeRoot = setRelativeRoot;
	scope[exposed_lib_name].setScriptDir = setScriptDir;
	
	// Expose properties
	scope[exposed_lib_name].defaults = default_options;
	scope[exposed_lib_name].targets = targets;
	
})(this);

/*
Tests:
require('/Editor/scripts/tab');
require('/Editor/scripts/tab')
	.then( r => {console.log('in resolve ret');console.log(r);} )
	.catch( err => {} );
require('/Editor/scripts/tab')
	.then( r => {console.log('in resolve ret');console.log(r);} )
	.catch( err => {} );
*/

/* See if we can get this to work (will need to figure out how arrays will be returned in the handler
function wait(t) {return new Promise( resolve => setTimeout(() => resolve(), t) );}
var temp_promise = require(['/Editor/scripts/tab-manager', '/Editor/scripts/tab']);
await wait(0);
console.log(temp_promise);
Promise.all(temp_promise).then( r => console.log(r) );
console.log('done');
//console.log([temp_promise[0]]);
*/

/*
// Typescript syntax:
import { Tab } from './tab';
import { TabManager } from './tab-manager';
await Promise.all([Tab, TabManager]);

// ES7 syntax:
[ {TabManager: TabManager}, {Tab: Tab} ] = await require([ '/Editor/scripts/tab-manager', '/Editor/scripts/tab' ]);
*/